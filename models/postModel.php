<?php  
//Fichero models/postModel.php

class Post{
	public $id;
	public $titulo;
	public $contenido;
	public $autor;
	public $fecha;
	public $url;

	public function __construct($registro){
		$this->id=$registro['id'];
		$this->titulo=$registro['titulo'];
		$this->contenido=$registro['contenido'];
		$this->autor=$registro['autor'];
		$this->fecha=$registro['fecha'];

		$titulo=preg_replace(array('/ /'), array('-'), strtolower(trim($this->titulo)));

		$this->url='entrada-'.$titulo.'-'.$this->id.'.html';
	}
} //Fin de la class Post
?>