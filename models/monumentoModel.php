<?php  
//Fichero models/centroModel.php

class Monumento{
	
	public $id;
	public $nombre;
	public $direccion;
	public $telefono;
	public $descripcion;
	public $estilo;
	public $fecha;

	// public $cp;
	// public $localidad;
	public $longitud;
	public $latitud;

	public function __construct($elemento){
		
		$this->id=$elemento->id;
		$this->nombre=$elemento->title;
		$this->direccion=$elemento->address;
		// $this->direccion=$elemento->address->{'street-address'};
		// $this->cp=$elemento->address->{'postal-code'};
		// @$this->localidad=$elemento->address->locality;
		// @$this->longitud=$elemento->location->longitude;
		// @$this->latitud=$elemento->location->latitude;
		@$this->telefono=$elemento->phone;
		$this->descripcion=$elemento->description;
		$this->estilo=$elemento->estilo;
		@$this->fecha=$elemento->datacion;

		$utm = new UTMRef($elemento->geometry->coordinates[0], $elemento->geometry->coordinates[1], "T", 30);
    	$ll = $utm->toLatLng();

		@$this->longitud=$ll->lng;
		@$this->latitud=$ll->lat;
	}
} //Fin de la class centro

?>