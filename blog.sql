-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-05-2018 a las 12:53:00
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `contenido` longtext COLLATE utf8_spanish_ci NOT NULL,
  `autor` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `blog`
--

INSERT INTO `blog` (`id`, `titulo`, `contenido`, `autor`, `fecha`) VALUES
(1, 'Estos son los barrios ''ricos'' en los que Zaragoza duplica el IBI a los garajes comunitarios', 'Se calcula que en Zaragoza hay unos 200 párquines comunitarios que se han visto afectados por el cambio normativo.\r\n<br><br>\r\n Varios garajes de la calle de José Oto, en el barrio de La Jota, entre los afectados\r\nVarios garajes de la calle de José Oto, en el barrio de La Jota, entre los afectadosGoogle Maps\r\nCon la recurrente justificación de que la medida afecta exclusivamente a inmuebles  propiedad de personas o familias de rentas altas, el gobierno de ZEC –con el apoyo  de PSOE y CHA, que ahora quieren rectificar- ha aplicado este año una subida del IBI a los garajes comunitarios con escritura colectiva y un valor catastral de más de un millón de euros que ha duplicado el importe a  miles de vecinos. \r\n<br><br>\r\nComo informó este lunes Heraldo.es el cambio afecta  los aparcamientos que  se inscribieron en el registro con una escritura colectiva y tienen un valor conjunto de más de un millón de euros. En este supuesto, la Gerencia del Catastro -dependiente del Ministerio de Hacienda-  cataloga dichos inmuebles como almacén de propiedad única. Se calcula que en Zaragoza hay unos 200 párquines comunitarios como estos, con miles de plazas en su conjunto, y todo ellos se han visto afectados  por el cambio normativo.', 'heraldo', '2018-05-29 00:00:00'),
(2, 'La peluquería con más juego de Zaragoza', 'La peluquería barbería de Daniel Vicente, en la calle de Conde de la Viñaza, 16, ubicada en el corazón del barrio de las Delicias, abrió sus puertas hace tres años. No es un establecimiento más y tampoco pasa inadvertido. Recientemente, su propietario ha decidido decorarlo con varios elementos relacionados con una de sus mayores aficiones: los videojuegos de los años 90.\r\n\r\n“Colecciono videojuegos retro desde que era pequeño porque mi hermano tenía un ordenador MSX, de aquellos que cargaban los juegos con cinta y con cartucho. De ahí me viene todo”, cuenta Daniel Vicente.\r\n\r\n \r\nLa Feria del Videojuego Primera Pantalla se llena de jugadores\r\nLeer más\r\nAl principio, abrió “con lo justo”, aunque reconoce que siempre tuvo en mente decorarla con temática de videojuegos de la década de los 90, principalmente (‘Tetris’, ‘Super Mario Land’, ‘Street Fighter II’, ‘Virtual Striker II’, ‘Time Crisis’...). “En España, no creo que haya muchas barberías de este estilo -comenta-, así que decidí juntar mis dos aficiones: el coleccionismo de videojuegos con mi otra pasión, la peluquería”.', 'heraldo', '2018-05-29 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
